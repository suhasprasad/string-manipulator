import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class StringManipulator {

     /*
        Method to remove non-alphabetic characters from string and return an uppercase string
     */
     public String cleanString(String str){
         return str.replaceAll("[^a-zA-Z]", "").toUpperCase();
     }

     /*
       Method checks if string cotains unique alphabets and if not will give out boolean false
      */
    public boolean hasUniqueChars(String word) {
         // clean up the word to remove any non-alphabetic characters

        String cleaned = word.replaceAll("[^a-zA-Z]", "").toUpperCase();
        // An integer to store presence/absence
        // of 26 characters using its 32 bits.
        int checker = 0;

        for (int i = 0; i < cleaned.length(); ++i)
        {
            int val = (cleaned.charAt(i)-'A');

            // If bit corresponding to current
            // character is already set
            if ((checker & (1 << val)) > 0)
                return false;

            // set bit in checker
            checker |= (1 << val);
        }

        return true;
    }
        /*
            Method returns weight of the string
        */
    public float getWeight(String word){
        String cleaned = cleanString(word);
        if(cleaned.isEmpty()){return 0;}
        float weight = 0;
        float size = cleaned.length();
        for (int i = 0; i < size; i++) {
            int asciiValue = cleaned.charAt(i);
            weight+=asciiValue;
        }
        return weight/size;
    }
    /*
            Method sorts an array of strings and returns a Map with the weight sorted in ascending order
     */

    public HashMap<String,Float> sortStrings(String[ ] words){
        // Read from array and sort by weight
        HashMap<String,Float> wordWeights = new HashMap<>();
        for(int i=0; i< words.length; i++){
            float weight = getWeight(words[i]);
            wordWeights.put((words[i]),weight);
        }

       HashMap<String,Float> sortedMap = wordWeights.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        return sortedMap;
     }

     /*
      Method to copy contents from an ArrayList into a String array
      */

     public String[] copyIntoArray(ArrayList<String> words){
         // copy ArrayList to String array
         int aSize= words.size();
         String wordArray[] = new String[aSize];
         for (int i = 0; i < aSize; i++)
         {
             wordArray[i] = words.get(i).toString();
         }
         return wordArray;
     }


     public static void main(String[] args){
         StringManipulator obj = new StringManipulator();
         //Setup to pick up property values

         Properties prop = new Properties();
         try {
             //load a properties file from class path, inside static method
             prop.load(Objects.requireNonNull(StringManipulator.class.getClassLoader().getResourceAsStream("resources/application.properties")));
         }
         catch (IOException ex) {
             ex.printStackTrace();
         }

         //Read from file
         String line = "";
         InputStream in = StringManipulator.class.getResourceAsStream(prop.getProperty("challenge.file.path"));
         // Using ArrayList as String array needs to be declared with a fixed size
         ArrayList words = new ArrayList<>();
         try (BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
            // Skip first line since it is a column name
             br.readLine();
             while ((line = br.readLine()) != null) {
                 // Add the words to a list
                words.add(line);
             }

             File file = new File(prop.getProperty("sorted.file.path"));
             FileOutputStream outputStream = new FileOutputStream(file);
             BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
             writer.write(prop.getProperty("column.fields"));
             writer.newLine();
             // Copy the Array List to String Array
             String[] wordsArray = obj.copyIntoArray(words);
             String output=" ";

             // Sort the words and get a Map of the cleaned words along with their weights
           HashMap<String, Float> weightedMap =obj.sortStrings(wordsArray);
           for(Map.Entry<String, Float> k: weightedMap.entrySet()){
               output=obj.cleanString(k.getKey())+","+obj.hasUniqueChars(k.getKey())+","+k.getValue();
               writer.write(output);
               writer.newLine();
           }
             writer.close();
         } catch (IOException e) {
             e.printStackTrace();
         }
     }
}
